
// Initialize Firebase
var config = {
    apiKey: "AIzaSyBG3KLVZ-O84QxZJxcmnk_-JLfc6K1JerQ",
    authDomain: "fbworkshopproject.firebaseapp.com",
    databaseURL: "https://fbworkshopproject.firebaseio.com",
    projectId: "fbworkshopproject",
    storageBucket: "fbworkshopproject.appspot.com",
    messagingSenderId: "576985573257"
};

firebase.initializeApp(config);

var database = firebase.database();

function toggleLedValue(){
    var ledState;
    database.ref('LED').once('value').then(snapshot => {
        ledState = !snapshot.val();
        database.ref('LED').set(ledState);
    })
}

database.ref('LDR-sensor').on('value', (data) => {
    var element = document.getElementById('ldr-value');
    element.innerHTML = data.val();
});